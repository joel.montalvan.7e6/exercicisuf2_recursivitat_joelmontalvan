import java.util.*

/*
* AUTHOR: Joel Montalvan Montiel
* DATE: 2023/01/15
* TITLE: Nombres creixents
*/

fun main (){
    val scanner = Scanner(System.`in`)
    println("Introduce un numero para saber si es creciente:")
    val numero = scanner.nextInt()
    val result= analizadorDeCreciente(numero)

    println(result)
}

fun analizadorDeCreciente(numero:Int):Boolean{
    if (numero/10 > 0) return numero%10 > (numero/10)%10 && analizadorDeCreciente(numero/10)
    else return true
}

