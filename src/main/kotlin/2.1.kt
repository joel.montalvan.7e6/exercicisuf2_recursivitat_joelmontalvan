import java.util.*

/*
* AUTHOR: Joel Montalvan Montiel
* DATE: 2023/01/11
* TITLE: Factorial
*/

fun main (){
    val scanner = Scanner(System.`in`)
    println("Introduce el numero para calcular su factorial:")
    val number = scanner.nextInt()
    val result = calculadorFactorial(number)

    println(result)

}

fun calculadorFactorial (number:Int): Long {
   if (number > 1){
       return number * calculadorFactorial(number - 1)
   }
    else return 1
}