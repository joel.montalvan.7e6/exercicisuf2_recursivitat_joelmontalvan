import java.util.*

/*
* AUTHOR: Joel Montalvan Montiel
* DATE: 2023/01/11
* TITLE: Doble factorial
*/

fun main (){
    val scanner = Scanner(System.`in`)
    println("Introduce el numero para calcular su doble factorial:")
    val number = scanner.nextInt()
    val result = calculadoraDobleFctorial(number)

    println(result)
}

fun calculadoraDobleFctorial (number:Int):Long {
    if (number > 1){
        return number * calculadoraDobleFctorial(number - 2)
    }
    else return 1
}