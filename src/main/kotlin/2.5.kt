import java.util.*

/*
* AUTHOR: Joel Montalvan Montiel
* DATE: 2023/01/15
* TITLE: Reducció de dígits
*/

fun main (){
    val scanner = Scanner(System.`in`)
    println("Introduce un numero para reducirlo al máximo:")
    val numero = scanner.nextInt()
    val result= reduccionDeNumero(numero)

    println(result)
}

fun reduccionDeNumero(numero:Int):Int{
    if (numero <= 9) return numero
    else return reduccionDeNumero(numero%10 + reduccionDeNumero(numero/10))
}