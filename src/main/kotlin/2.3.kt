import java.util.*

/*
* AUTHOR: Joel Montalvan Montiel
* DATE: 2023/01/11
* TITLE: Nombre de dígits
*/

fun main (){
    val scanner = Scanner(System.`in`)
    println("Introduce un numero para contar sus digitos:")
    val numero = scanner.nextInt()
    val result = contadorNumeroDeDigitos(numero)

    println(result)
}

fun contadorNumeroDeDigitos (numero:Int):Int{
    if (numero > 9){
        return 1 + contadorNumeroDeDigitos(numero/10)
    }
    else return 1
}